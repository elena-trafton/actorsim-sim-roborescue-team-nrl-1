##Communication

Communication is handled by the messageManager for each of the agents. 

####Sending

To send a message, you first have to figure out what type of message you need to send. Each message can also have a priority. To send the messages, use `messageManager.add(message)`.

######Agent Specific Messages

For the police, fire brigade, and ambulance there are various agent specific messages that are meant to be used to document what the agent is currently doing. The syntax for these are:
* For police
    * `new MessagePoliceForce(boolean isRadio, PoliceForce police, int action, EntityID target)`
    * `new MessagePoliceForce(boolean isRadio, StandardMessagePriority sendingPriority, PoliceForce police, int action, EntityID target)`
    * `new MessagePoliceForce(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)
* For fire brigade
    * `new MessageFireBrigade(boolean isRadio, FireBrigade fireBrigade, int action, EntityID target)`
    * `new MessageFireBrigade(boolean isRadio, StandardMessagePriority sendingPriority, FireBrigade fireBrigade, int action, EntityID target)`
    * `new MessageFireBrigade(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For ambulance
    * `new MessageAmbulanceTeam(boolean isRadio, AmbulanceTeam ambulance, int action, EntityID target)`
    * `new MessageAmbulanceTeam(boolean isRadio, StandardMessagePriority sendingPriority, AmbulanceTeam ambulance, int action, EntityID target)`
    * `new MessageAmbulanceTeam(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`

Note: since the messages send the agent themselves, other agents will be able to access the other agent information such as position or buriedness from this. Also, the actions are constant ints contained within each of the message classes.

######Information Messages

These messages are for notifying other agents of important information. There specific use is to inform about buildings (can also notify that the building is on fire), civilian locations and conditions, and roads (whether they are blockaded or not. The syntax for them are:
* For buildings
    * `new MessageBuilding(boolean isRadio, Building building)`
    * `new MessageBuilding(boolean isRadio, StandardMessagePriority sendingPriority, Building building)`
    * `new MessageBuilding(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For civilians
    * `new MessageCivilian(boolean isRadio, Civilian civilian)`
    * `new MessageCivilian(boolean isRadio, StandardMessagePriority sendingPriority, Civilian civilian)`
    * `new MessageCivilian(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For roads
    * `new MessageRoad(boolean isRadio, Road road, Blockade blockade, Boolean isPassable, boolean isSendBlockadeLocation)`
    * `new MessageRoad(boolean isRadio, StandardMessagePriority sendingPriority, Road road, Blockade blockade, Boolean isPassable, boolean isSendBlockadeLocation)`
    * `new MessageRoad(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
    
######Commands

These messages are for telling other agents to do specific things. There is one for each of the non-dispatch agents as well as a command for scouting. The syntax for them are:
* For police
    * `new CommandPolice(boolean isRadio, EntityID toID, EntityID targetID, int action)`
    * `new CommandPolice(boolean isRadio, StandardMessagePriority sendingPriority, EntityID toID, EntityID targetID, int action)`
    * `new CommandPolice(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For fire brigade
    * `new CommandFire(boolean isRadio, EntityID toID, EntityID targetID, int action)`
    * `new CommandFire(boolean isRadio, StandardMessagePriority sendingPriority, EntityID toID, EntityID targetID, int action)`
    * `new CommandFire(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For ambulance
    * `new CommandAmbulance(boolean isRadio, EntityID toID, EntityID targetID, int action)`
    * `new CommandAmbulance(boolean isRadio, StandardMessagePriority sendingPriority, EntityID toID, EntityID targetID, int action)`
    * `new CommandAmbulance(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`
* For scouting
    * `new CommandScout(boolean isRadio, EntityID toID, EntityID targetID, int range)`
    * `new CommandScout(boolean isRadio, StandardMessagePriority sendingPriority, EntityID toID, EntityID targetID, int range)`
    * `new CommandScout(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`

Note: Like the agent specific messages, the actions are constant ints that can be accessed from the class. Any agent can send these messages. Also, there are deprecated variants to each of the commands.

######Reports

These are to report on the progress done for a specific action. These are helpful if you are using command structures. The syntax for them are:
* `new MessageReport(boolean isRadio, boolean isDone, boolean isBroadcast, EntityID fromID)`
* `new MessageReport(boolean isRadio, StandardMessagePriority sendingPriority, boolean isDone, boolean isBroadcast, EntityID fromID)`
* `new MessageReport(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`

######Dummy Messages

These are most likely sent during the scenarios with low communication settings. The syntax to create them are:
* `new MessageDummy(boolean isRadio, int test)`
* `new MessageDummy(boolean isRadio, StandardMessagePriority sendingPriority, int test)`
* `new MessageDummy(boolean isRadio, int from, int ttl, BitStreamReader bitStreamReader)`

####Receiving

Agents can receive all the messages that are sent. In order to receive the messages, use `messageManager.getReceivedMessageList()`. If you want to filter it by the types of messages, you can use `messageManager.getReceivedMessageList(class)`. Do note, these are lists of CommunicationMessage, which are the super class of the super class for most of the messages you are sending. If you filtered the list by class, casting shouldn't cause any problems.