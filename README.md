This repository contains the ActorSim connector 
for the Robocup Roborescue Simulation League.

For a description of the ActorSim core library, 
see the ACTORSIM_CORE_HOME/src/main/site/README.md.

For complete details about this connector, start with
[actorsim-sim-roborescue_centralized_dispatcher/src/main/site/INDEX.md](actorsim-sim-roborescue_centralized_dispatcher/src/main/site/INDEX.md)
