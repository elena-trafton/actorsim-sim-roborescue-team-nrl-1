package nrl.actorsim.roborescue;

import java.util.List;

import rescuecore2.worldmodel.EntityID;

public class PathWithEndPosition {
	private List<EntityID> path;
	private int endX;
	private int endY;
	public PathWithEndPosition(List<EntityID> path, int endX, int endY) {
		this.path = path;
		this.endX = endX;
		this.endY = endY;
	}
	public List<EntityID> getPath() {
		return path;
	}
	public int getEndX() {
		return endX;
	}
	public int getEndY() {
		return endY;
	}
	
}
