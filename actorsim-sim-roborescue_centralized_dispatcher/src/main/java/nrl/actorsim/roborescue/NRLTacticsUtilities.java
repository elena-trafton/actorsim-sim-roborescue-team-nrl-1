package nrl.actorsim.roborescue;

import java.util.*;

import org.slf4j.LoggerFactory;

import adf.agent.communication.standard.bundle.information.MessageAmbulanceTeam;
import adf.agent.communication.standard.bundle.information.MessageFireBrigade;
import adf.agent.communication.standard.bundle.information.MessagePoliceForce;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.component.communication.CommunicationMessage;
import adf.component.module.algorithm.PathPlanning;
import adf.debug.WorldViewLauncher;
import rescuecore2.misc.Pair;
import rescuecore2.misc.geometry.Point2D;
import rescuecore2.misc.geometry.Vector2D;
import rescuecore2.standard.entities.Area;
import rescuecore2.standard.entities.Blockade;
import rescuecore2.standard.entities.Human;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

import static java.util.Collections.EMPTY_LIST;
import static nrl.actorsim.roborescue.CentralizedDispatcher.NULL_ENTITY_ID;


public class NRLTacticsUtilities {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(NRLTacticsUtilities.class);

	public final static int TIMESTEPS_UNTIL_AGENTS_CAN_MOVE = 3;
	
	private final static int MINIMUM_DISTANCE_TO_BE_CONSIDERED_MOVEMENT = 20;
	
	public static List<EntityID> findShortestPathToAreaInCollection(PathPlanning pathPlanning, EntityID startingPosition, Collection<EntityID> targets){
		List<EntityID> shortestPath = null;
		double shortestDistance = Double.MAX_VALUE;
		for(EntityID target : targets) {
			pathPlanning.setFrom(startingPosition);
			pathPlanning.setDestination(target);
			List<EntityID> path = pathPlanning.calc().getResult();
			double distance = pathPlanning.getDistance();
			if((distance < shortestDistance)
                    && (path != null)
                    && (path.size() >= 1)) {
				shortestPath = path;
				shortestDistance = distance;
			}
		}
		return shortestPath;
	}
	
	public static Pair<EntityID, List<EntityID>> calculateClosestTarget(PathPlanning pathPlanning, EntityID source, Collection<EntityID> targets){
		EntityID closest = NULL_ENTITY_ID;

		List<EntityID> shortestPath = null;
		double shortestDistance = Double.MAX_VALUE;
		for(EntityID target : targets) {
			pathPlanning.setFrom(source);
			pathPlanning.setDestination(target);
			List<EntityID> path = pathPlanning.calc().getResult();
			double distance = pathPlanning.getDistance();
			if((distance < shortestDistance)
                    && (path != null)
                    && (path.size() >= 1)) {
				closest = target;
				shortestPath = path;
				shortestDistance = distance;
			}
		}
		return new Pair<>(closest, shortestPath);
	}

	public static class ClosestResult {
        EntityID closestID = NULL_ENTITY_ID;
        List<EntityID> shortestPath = EMPTY_LIST;
		double distance = Double.MAX_VALUE;

		public RRWorldObject closestAgent = RRWorldObject.NULL_RR_WORLD_OBJECT;
        public RRWorldObject closestPlace = RRWorldObject.NULL_RR_WORLD_OBJECT;
	}
	public static ClosestResult calculateClosestTarget(PathPlanning pathPlanning, RRWorldObject source, Collection<RRWorldObject> targets) {
		ClosestResult result = new ClosestResult();
		EntityID sourceLocation = source.location;
		List<EntityID> targetLocations = new ArrayList<>();
		for (RRWorldObject target : targets) {
		    if (target.location != NULL_ENTITY_ID) {
                targetLocations.add(target.location);
            }
		}

		if (targetLocations.size() > 0) {
            pathPlanning.setFrom(sourceLocation);
            pathPlanning.setDestination(targetLocations);
            try {
                result.shortestPath = pathPlanning.calc().getResult();
                if ((result.shortestPath != null)
                        && (result.shortestPath.size() > 0)) {
					result.distance = pathPlanning.getDistance();
                    int lastIndex = result.shortestPath.size() - 1;
                    EntityID location = result.shortestPath.get(lastIndex);
                    for (RRWorldObject target : targets) {
                        if (location.equals(target.location)) {
                            result.closestID = target.getEntityId();
                            break;
                        }
                    }
                }
            } catch (NullPointerException e) {
                logger.error("No solution was found; are you sure the problem is solvable? StackTrace:", e);
            }
        } else {
		    logger.debug("The target list was empty - was the location set for these targets?");
        }

		return result;
	}

	public static PathWithEndPosition findShortestPathToHumanIDInCollectionWithEndPoint(WorldInfo worldInfo, PathPlanning pathPlanning, EntityID startingPosition, Collection<EntityID> humanIDs) {
		List<EntityID> shortestPath = null;
		Pair<Integer, Integer> endPosition = worldInfo.getLocation(startingPosition);
		double shortestDistance = Double.MAX_VALUE;
		for(EntityID humanID : humanIDs) {
			StandardEntity human = worldInfo.getEntity(humanID);
			if(!(human instanceof Human)) {
				throw new IllegalArgumentException("HumanIDs contained non-human ID");
			}
			pathPlanning.setFrom(startingPosition);
			pathPlanning.setDestination(((Human)human).getPosition());
			List<EntityID> path = pathPlanning.calc().getResult();
			double distance = pathPlanning.getDistance();
			if(distance < shortestDistance
					&& path != null
					&& path.size() >= 1) {
				shortestPath = path;
				shortestDistance = distance;
				endPosition = worldInfo.getLocation(humanID);
			}
		}
		return new PathWithEndPosition(shortestPath, endPosition.first(), endPosition.second());
	}
	
	
	
	public static PathWithEndPosition findShortestPathToAreaIDInCollectionWithEndPoint(WorldInfo worldInfo, PathPlanning pathPlanning, EntityID startingPosition, Collection<EntityID> targets) {
		List<EntityID> shortestPath = null;
		Pair<Integer, Integer> endPosition = worldInfo.getLocation(startingPosition);
		double shortestDistance = Double.MAX_VALUE;
		for(EntityID target : targets) {
			pathPlanning.setFrom(startingPosition);
			pathPlanning.setDestination(target);
			List<EntityID> path = pathPlanning.calc().getResult();
			double distance = pathPlanning.getDistance();
			if(distance < shortestDistance
					&& path != null
					&& path.size() >= 1) {
				shortestPath = path;
				shortestDistance = distance;
				endPosition = worldInfo.getLocation(target);
			}
			
		}
		
		return new PathWithEndPosition(shortestPath, endPosition.first(), endPosition.second());
		
	}

	public static EntityID getSenderPosition(CommunicationMessage message) {
		if(message.getClass() == MessageAmbulanceTeam.class) {
			MessageAmbulanceTeam activityMessage = (MessageAmbulanceTeam)message;
			return activityMessage.getPosition();

		}
		else if(message.getClass() == MessagePoliceForce.class) {
			MessagePoliceForce activityMessage = (MessagePoliceForce)message;
			return activityMessage.getPosition();
		}
		else if(message.getClass() == MessageFireBrigade.class){
			MessageFireBrigade activityMessage = (MessageFireBrigade)message;
			return activityMessage.getPosition();
		}
		return null;
	}

	/**
	 * A convenience method to show the timestep for the agent.
	 * 
	 * @param isVisualDebug
	 * @param agentInfo
	 * @param worldInfo
	 * @param scenarioInfo
	 */
	public static void showTimestep(boolean isVisualDebug, AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo) {
		if(isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}
	}

	public static double getDistanceDouble(double fromX, double fromY, double toX, double toY) {
		double dx = Math.abs(toX - fromX);
		double dy = Math.abs(toY - fromY);
		return Math.hypot(dx, dy);
	}
	
	public static int getDistanceIntegerPair(Pair<Integer, Integer> from, Pair<Integer, Integer> to) {
		int dx = Math.abs(from.first() - to.first());
		int dy = Math.abs(from.second() - to.second());
		return (int)Math.hypot(dx, dy);
	}

	public static boolean isInside(double positionX, double positionY, int[] apexes) {
		Point2D positionPoint = new Point2D(positionX, positionY);
		Vector2D vector1 = new Point2D(apexes[apexes.length - 2], apexes[apexes.length - 1]).minus(positionPoint);
		Vector2D vector2 = new Point2D(apexes[0], apexes[1]).minus(positionPoint);
		double theta = getAngle(vector1, vector2);
		
		for(int i = 0; i < apexes.length - 2; i += 2) {
			vector1 = new Point2D(apexes[i], apexes[i + 1]).minus(positionPoint);
			vector2 = new Point2D(apexes[i + 2], apexes[i + 3]).minus(positionPoint);
			theta += getAngle(vector1, vector2);
		}
		return Math.round(Math.abs((theta / 2) / Math.PI)) >= 1l;
	}
	
	public static double getAngle(Vector2D vector1, Vector2D vector2) {
		double flag = vector1.getX() * vector2.getY() - vector1.getY() * vector2.getX();
		double angle = Math.acos((vector1.getX() * vector2.getX() + vector1.getY() * vector2.getY()) 
				/ (vector1.getLength() * vector2.getLength()));
		if(flag > 0) {
			return angle;
		}
		if(flag < 0) {
			return -angle;
		}
		return 0.0d;
	}
	
	public static boolean isHumanStuck(WorldInfo worldInfo, Human human) {
		int humanX = human.getX();
		int humanY = human.getY();
		StandardEntity positionEntity = worldInfo.getPosition(human);
		if(positionEntity.getStandardURN() == StandardEntityURN.ROAD) {
			Road road = (Road)positionEntity;
			if(road.isBlockadesDefined()
					&& road.getBlockades().size() > 0) {
				for(Blockade blockade : worldInfo.getBlockades(road)) {
					if(blockade == null || !blockade.isApexesDefined()) {
						continue;
					}
					if(NRLTacticsUtilities.isInside(humanX, humanY, blockade.getApexes())) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	
	public static boolean hadEffectivelyNotMoved(Pair<Integer, Integer> before, Pair<Integer, Integer> after) {
		
		return getDistanceIntegerPair(before, after) < MINIMUM_DISTANCE_TO_BE_CONSIDERED_MOVEMENT;
	}
	
	
	public static Collection<Blockade> getAllBlockadesFromPositionAndSurroundingPositions(WorldInfo worldInfo, Area area){
		Collection<Blockade> blockades = worldInfo.getBlockades(area);
		for(EntityID neighborID : area.getNeighbours()) {
			blockades.addAll(worldInfo.getBlockades(neighborID));
		}
		return blockades;
	}
	
	public static Set<Integer> getAllBlockadeIDsFromPositionAndSurroundingPositions(WorldInfo worldInfo, Area area){
		Collection<Blockade> blockades = getAllBlockadesFromPositionAndSurroundingPositions(worldInfo, area);
		Set<Integer> blockadeIDs = new HashSet<Integer>();
		for(Blockade blockade : blockades) {
			blockadeIDs.add(blockade.getID().getValue());
		}
		return blockadeIDs;
	}
	
	public static Set<Integer> convertEntityIDSetToIntegerSet(Set<EntityID> set){
		Set<Integer> newSet = new HashSet<Integer>();
		for(EntityID id : set) {
			newSet.add(id.getValue());
		}
		return newSet;
	}
	
	private static byte[] shortenByteArray(byte[] arr) {
		int indexOf0 = -1;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == 0) {
				indexOf0 = i;
				break;
				
			}
		}
		if(indexOf0 == -1) {
			return arr;
		}
		byte[] newArr = new byte[indexOf0];
		for(int i = 0; i < indexOf0; i ++) {
			newArr[i] = arr[i];
		}
		return newArr;
	}

}
