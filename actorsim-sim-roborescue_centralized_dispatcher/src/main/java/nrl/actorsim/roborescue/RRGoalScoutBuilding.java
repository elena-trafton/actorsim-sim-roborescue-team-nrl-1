package nrl.actorsim.roborescue;

import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import adf.agent.communication.standard.bundle.centralized.CommandFire;
import adf.agent.communication.standard.bundle.centralized.CommandPolice;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;

import java.util.Collections;

public class RRGoalScoutBuilding extends RRGoalBase {
    RRGoalScoutBuilding() {
        super(CommandAmbulance.ACTION_MOVE,
                RRPlanningDomain.EXPLORED_BUILDING,
                CompletionCondition.PRESENT_IN_MEMORY,
                Collections.emptyList());
        //NB: Assuming that Fire and Police have the same ACTION_MOVE id.
        //    The assertions below ensure a halt if that is changed.
        //noinspection ConstantConditions
        assert(CommandAmbulance.ACTION_MOVE == CommandPolice.ACTION_MOVE);
        //noinspection ConstantConditions
        assert(CommandAmbulance.ACTION_MOVE == CommandFire.ACTION_MOVE);
    }

    private RRGoalScoutBuilding(RRGoalScoutBuilding template, RRWorldObject building) {
        super(template, building);
    }

    @Override
    public RRGoalBase instance(WorldObject obj) {
        if (obj instanceof RRWorldObject) {
            if (isBuilding(obj)) {
                return new RRGoalScoutBuilding(this, (RRWorldObject) obj);
            }
        }
        return NULL_RR_BASE_GOAL;
    }

    @Override
    public boolean matchesVariable(StateVariable sv) {
        return super.matchesVariable(sv)
                && sv.hasBindings()
                && isBuilding(sv.getBinding(0));
    }

    private boolean isBuilding(WorldType other) {
        return other.equalsOrInheritsFromType(RRPlanningDomain.BUILDING);
    }
}
