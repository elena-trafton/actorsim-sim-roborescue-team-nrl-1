package nrl.actorsim.roborescue;

import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.StandardMessage;
import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import adf.agent.communication.standard.bundle.centralized.CommandFire;
import adf.agent.communication.standard.bundle.centralized.CommandPolice;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsAmbulanceCentre;
import nrl.actorsim.domain.PredicateStatement;
import nrl.actorsim.domain.StatePredicate;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.GoalMemoryOptions;
import nrl.actorsim.cognitive.CognitiveCycleWorker;
import nrl.actorsim.memory.WorkingMemory;
import nrl.actorsim.domain.PlanningDomain;
import nrl.actorsim.cognitive.RankStrategiesByGoalOrdering;
import nrl.actorsim.cognitive.RankStrategiesInterface;
import nrl.actorsim.utils.NamedImpl;
import nrl.actorsim.roborescue.NRLTacticsUtilities.ClosestResult;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;
import test_team.module.algorithm.AStarPathPlanning;

import java.util.*;

import static nrl.actorsim.roborescue.RRWorldObject.NULL_RR_WORLD_OBJECT;

public class CentralizedDispatcher extends NamedImpl {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CentralizedDispatcher.class);
    public static EntityID NULL_ENTITY_ID = new EntityID(-1);

    public static boolean isInitialized = false;

    private static CentralizedDispatcher instance;

    NRLAgentStateOptions stateOptions;

    private final NRLAgentState state;
    WorkingMemory memory;
    private static DispatcherWorker worker;

    PathPlanning pathPlanning;
    final List<StandardMessage> commandMessages = Collections.synchronizedList(new ArrayList<>());

    List<PassthroughDispatcher> ambulances = new ArrayList<>();
    private boolean isShuttingDown = false;

    private Map<EntityID, StandardMessage> agentCommands = new HashMap<>();
    Map<EntityID, GoalLifecycleNode> agentGoals = new HashMap<>();
    private boolean skipFirstStep = true;

    // =============================================================
    // region<Constructors and Initializers>

    private CentralizedDispatcher(RROptions rrOptions) {
        shortName = "Dispatcher";

        stateOptions = new NRLAgentStateOptions();
        stateOptions.updateUsingCommunications = true;
        stateOptions.parseRoadsAndBuildings = true;
        stateOptions.parseCivilianMessages = true;
        stateOptions.parsePoliceMessages = true;
        stateOptions.parseAmbulanceMessages = true;
        stateOptions.parseFireBrigadeMessages = true;
        stateOptions.printWorldInfo = false;
        stateOptions.printMessagesSeen = true;
        state = new NRLAgentState(this, stateOptions);

        this.pathPlanning = new AStarPathPlanning(rrOptions.agentInfo, rrOptions.worldInfo,
        rrOptions.scenarioInfo, rrOptions.moduleManager, rrOptions.developData);

        GoalMemoryOptions goalMemoryOptions = GoalMemoryOptions.builder().name("RRGoals").build();
        WorkingMemory.Options memoryOptions = WorkingMemory.Options.builder()
                .goalMemoryOptions(goalMemoryOptions)
                .build();
        memory = new WorkingMemory(memoryOptions);
        logger.info("Initialized options to {}", memoryOptions);

        PlanningDomain.Options domainOptions = PlanningDomain.Options.builder()
                .name("roborescue")
                .closedWorldAssumption()
                .build();
        RRPlanningDomain domain = new RRPlanningDomain(domainOptions);

        RRStrategies strategies = new RRStrategies(memory);
        strategies.addStrategies(memory);
        RankStrategiesInterface strategyRanker = new RankStrategiesByGoalOrdering(memory);

        CognitiveCycleWorker.Options workerOptions = CognitiveCycleWorker.Options.builder()
                .workingMemory(memory)
                .memoryUpdateLock(state)
                .ranker(strategyRanker)
                .shortName("DW")
                .build();
        worker = new DispatcherWorker(workerOptions);
        worker.start();
		logger.info("{}: Initialized", shortName);
    }

    public static CentralizedDispatcher initialize(RROptions rrOptions) {
        if (instance == null) {
            CentralizedDispatcher newInstance = new CentralizedDispatcher(rrOptions);
            newInstance.shortName = "Dispatcher";
            newInstance.memory.showGoalMemoriesIfEnabled();

            instance = newInstance;
            isInitialized = true;
        } else {
            String message = "Attempting to initialize the Connector a second time is unsupported.  Call shutdown first!";
            logger.error(message);
            throw new UnsupportedOperationException(message);
        }
        return instance;
    }

    public static boolean isInitialized() {
        return instance != null;
    }

    public static CentralizedDispatcher getInstance() {
        if (! CentralizedDispatcher.isInitialized()) {
            String message = "You must initialize this connector before using it.";
            throw new UnsupportedOperationException(message);
        }
        return instance;
    }

    // endregion
    // =============================================================

    // =============================================================
    // region<Accessors>

    public NRLAgentState getState() {
        return state;
    }

    public WorkingMemory getWorkingMemory() {
        return memory;
    }

    public static void enableStopOnBreakPointForWorker() {
        worker.enableStopOnBreakpoint();
    }

    public static void disableStopOnBreakPointForWorker() {
        worker.disableStopOnBreakpoint();
    }


    public void register(PassthroughDispatcher commandable) {
        if (commandable instanceof TacticsAmbulanceCentre) {
            ambulances.add(commandable);
            logger.info("{}: registered ambulance {}", getShortName(), commandable.getShortName());
        } else {
            logger.error("Cannot register commandable {} of type {}", commandable.getShortName(), commandable.getClass());
        }
    }

    public void addMessages(MessageManager messageManager) {
        synchronized (commandMessages) {
            Iterator<StandardMessage> iter = commandMessages.iterator();
            while (iter.hasNext()) {
                StandardMessage message = iter.next();
                messageManager.addMessage(message);
                logger.debug("added command message {}", message);
                iter.remove();
            }
        }
    }

    public RRWorldObject getObject(EntityID id) {
        RRWorldObject worldObject = NULL_RR_WORLD_OBJECT;
        if (state.entityIDWorldObjectMap.containsKey(id)) {
            worldObject = state.entityIDWorldObjectMap.get(id);
        }
        return worldObject;
    }

    public RRWorldObject getObject(String id) {
        RRWorldObject worldObject = NULL_RR_WORLD_OBJECT;
        try {
            EntityID entityID = new EntityID(Integer.parseInt(id));
            worldObject = getObject(entityID);
        } catch (NumberFormatException e) {
            logger.error("Could not convert id '{}' into valid integer.", id, e);
        }
        return worldObject;
    }

    // endregion
    // ===========================================================

    // ===========================================================
    // region<Update details for Roborescue code>

    void update(TacticsCommandable updater, WorldInfo worldInfo, MessageManager messageManager) {
        logger.debug("=========================================================================");
        logger.debug("{}: Updating state", getShortName());
        if (! state.initialized) {
            if (skipFirstStep) {
                skipFirstStep = false;
                return;
            }
            state.initialize(worldInfo);
        }
        synchronized (state) {
            state.update(worldInfo, messageManager);
        }
        worker.notifyWorkerThereIsWork();
    }
    // endregion
    // ===========================================================

    // ===========================================================
    // region<Available Agents and Assignments>

    /***
     * Returns a result of the closestID scouting agent (or NULL_RR_WORLD_OBJECT if none is available) and
     * the path to that agent.
     *
     * @param place the place to search for closest
     */
    public ClosestResult getClosestAvailableScoutingAgent(RRWorldObject place) {
        RRWorldObject closest = NULL_RR_WORLD_OBJECT;
        List<RRWorldObject> availableScouts = getAvailableAgents();
        ClosestResult result = NRLTacticsUtilities.calculateClosestTarget(pathPlanning, place, availableScouts);
        if (result.closestID != NULL_ENTITY_ID) {
            result.closestAgent = state.entityIDWorldObjectMap.get(result.closestID);
        }
        return result;
    }

    public NRLTacticsUtilities.ClosestResult getClosestPlace(RRWorldObject scout, Collection<RRWorldObject> places) {
        ClosestResult result = NRLTacticsUtilities.calculateClosestTarget(pathPlanning, scout, places);
        if (result.closestID != NULL_ENTITY_ID) {
            if (state.entityIDWorldObjectMap.containsKey(result.closestID)) {
                result.closestPlace = state.entityIDWorldObjectMap.get(result.closestID);
            } else {
                logger.error("The id {} is not found in the entityIDWorldObjectMap", result.closestID);
            }
        }
        return result;
    }

    public List<RRWorldObject> getAvailableAgents(StandardEntityURN... allowedAgentTypeFilter) {
        return getAvailableAgents(Arrays.asList(allowedAgentTypeFilter));
    }

    public List<RRWorldObject> getAvailableAgents(List<StandardEntityURN> allowedAgentTypeFilter) {
        List<RRWorldObject> available = new ArrayList<>();
        for (EntityID entityID : state.agentTypeMap.keySet()) {
            if (! state.entityIDWorldObjectMap.containsKey(entityID)) {
                StandardEntityURN urn = state.entityIDTypeMap.get(entityID);
                logger.error("{}: entityID {} with type {} does not have an associated world object (addOrUpdate it during updateWorkingMemory()?)", getShortName(), entityID, urn.name());
                continue;
            }

            RRWorldObject agentObject = state.entityIDWorldObjectMap.get(entityID);

            if ((allowedAgentTypeFilter.size() > 0)
                && (! allowedAgentTypeFilter.contains(agentObject.getUrn()))) {
                logger.trace("The agent type {} was filtered.", agentObject.getUrn());
                continue;
            }

            StatePredicate tmpAssigned = RRPlanningDomain.ASSIGNED.copier().build(agentObject);
            PredicateStatement statement = tmpAssigned.statement();
            if (! memory.contains(statement)) {
                if (! state.humansBuried.containsKey(entityID)) {
                    available.add(agentObject);
                }
            }
        }
        return available;
    }

    public boolean attemptAssignment(RRGoalBase baseGoal) {
        RRWorldObject suggestedAssignment = baseGoal.getSuggestedAssignment();
        StatePredicate assigned = RRPlanningDomain.ASSIGNED.copier().build(suggestedAssignment);
        PredicateStatement statement = assigned.statement();
        boolean assignmentSucceeded = false;
        logger.debug("Attempting to assign {} to agent {}", baseGoal, suggestedAssignment);
        if (! memory.contains(statement)) {
            logger.debug("Agent {} is not assigned to a goal, so assigning it to {}", suggestedAssignment, baseGoal);
            baseGoal.assign(suggestedAssignment);
            memory.updateOrCloneThenAdd(statement, state.currentTime);
            assignmentSucceeded = true;
        } else {
            logger.debug("Agent {} is already assigned", suggestedAssignment);
        }
        return assignmentSucceeded;
    }

    public void attemptReassignment(RRGoalBase baseGoal) {
        RRWorldObject newAssignment = baseGoal.getReassigment();
        StatePredicate assigned = RRPlanningDomain.ASSIGNED.copier().build(newAssignment);
        PredicateStatement statement = assigned.statement();
        logger.debug("Attempting to assign {} to {}", newAssignment, baseGoal);
        if (agentGoals.containsKey(newAssignment.getEntityId())) {
            RRGoalBase misAssignedGoal = (RRGoalBase) agentGoals.get(newAssignment.getEntityId());
            logger.debug("Unassigning agent {} from {}", newAssignment, misAssignedGoal);
            unassign(misAssignedGoal);
        }

        logger.debug("Assigning agent {} to {}", newAssignment, baseGoal);
        baseGoal.assign(newAssignment);
        baseGoal.clearReassigment();
        memory.updateOrCloneThenAdd(statement, state.currentTime);
    }

    public boolean executeAssignment(RRGoalBase baseGoal) {
        boolean messagePrepared = false;
        logger.debug("Executing assignment for {}", baseGoal);

        if (baseGoal.hasBeenReassigned()) {
            logger.debug("Reassigning agent to match server assignment.");
            attemptReassignment(baseGoal);
        }

        EntityID entityID = baseGoal.getAssignedByDispatcher().getEntityId();
        StandardEntityURN urn = state.entityIDTypeMap.get(entityID);
        StandardMessage message = null;

        switch (urn) {
            case FIRE_BRIGADE:
                message = new CommandFire(true, entityID, baseGoal.getEntity().getEntityId(), baseGoal.getAgentAction());
                messagePrepared = true;
                break;
            case AMBULANCE_TEAM:
                message = new CommandAmbulance(true, entityID, baseGoal.getEntity().getEntityId(), baseGoal.getAgentAction());
                messagePrepared = true;
                break;
            case POLICE_FORCE:
                message = new CommandPolice(true, entityID, baseGoal.getEntity().getEntityId(), baseGoal.getAgentAction());
                messagePrepared = true;
                break;
        }
        if (message != null) {
            agentCommands.put(entityID, message);
            agentGoals.put(entityID, baseGoal);
            synchronized (commandMessages) {
                commandMessages.add(message);
            }
        }

        return messagePrepared;
    }

    public void unassign(RRGoalBase subgoal) {
        RRWorldObject currentAssignment = subgoal.getAssignedByDispatcher();
        if (currentAssignment != NULL_RR_WORLD_OBJECT) {
            StatePredicate assigned = RRPlanningDomain.ASSIGNED.copier().build(currentAssignment);
            PredicateStatement statement = assigned.statement();
            logger.debug("Attempting to unassign {} from agent {}", subgoal, currentAssignment);
            if (memory.contains(statement)) {
                logger.debug("Goal {} is assigned to {}, so unassigning it and updating memory", subgoal, currentAssignment);
                subgoal.unassign();
                memory.remove(statement, state.currentTime);
            }
            agentCommands.remove(currentAssignment.getEntityId());
            agentGoals.remove(currentAssignment.getEntityId());
        }
    }

    public StandardMessage getCommandMessage(EntityID id) {
        StandardMessage message = null;
        if (agentCommands.containsKey(id)) {
            message = agentCommands.get(id);
        }
        return message;
    }

    // endregion
    // ===================================================

    // ===================================================
    // region<Cognitive Cycle>

    private class DispatcherWorker extends CognitiveCycleWorker {
        public DispatcherWorker(CognitiveCycleWorker.Options options) {
            super(options);
        }

        @Override
        protected void updateWorkingMemory() {
            logger.debug("Updating working memory from latest state");
            state.updateMemoryFromCurrentState();
            logger.debug("Completed updating working memory from latest state");
        }

    }
    // endregion
    // ===================================================
}

// ===========================================================
// region<RROptions>

class RROptions {
    AgentInfo agentInfo;
    WorldInfo worldInfo;
    ScenarioInfo scenarioInfo;
    ModuleManager moduleManager;
    DevelopData developData;

    public RROptions(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        this.agentInfo = agentInfo;
        this.worldInfo = worldInfo;
        this.scenarioInfo = scenarioInfo;
        this.moduleManager = moduleManager;
        this.developData = developData;
    }
}
// endregion
// ===========================================================

