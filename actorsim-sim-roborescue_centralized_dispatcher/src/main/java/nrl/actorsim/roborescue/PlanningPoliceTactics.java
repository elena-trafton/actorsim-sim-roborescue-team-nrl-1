package nrl.actorsim.roborescue;

import java.util.HashSet;
import java.util.Set;

import adf.agent.action.Action;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.information.MessageAmbulanceTeam;
import adf.agent.communication.standard.bundle.information.MessageFireBrigade;
import adf.agent.communication.standard.bundle.information.MessagePoliceForce;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.communication.CommunicationMessage;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsPoliceForce;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.standard.entities.PoliceForce;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;
import test_team.module.algorithm.AStarPathPlanning;

public class PlanningPoliceTactics extends TacticsPoliceForce{

	private PathPlanning pathPlanning;
	private MessageTool messageTool;

	private Boolean isVisualDebug;
	
	private Set<EntityID> unexploredRoads;
	private Set<EntityID> unexploredBuildings;

	@Override
	public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
		worldInfo.indexClass(
				StandardEntityURN.ROAD,
				StandardEntityURN.HYDRANT,
				StandardEntityURN.BUILDING,
				StandardEntityURN.REFUGE,
				StandardEntityURN.BLOCKADE
				);

		this.messageTool = new MessageTool(scenarioInfo, developData);

		this.isVisualDebug = (scenarioInfo.isDebugMode());

		this.pathPlanning = new AStarPathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
		this.unexploredRoads = new HashSet<EntityID>(worldInfo.getEntityIDsOfType(StandardEntityURN.ROAD));
		this.unexploredBuildings = new HashSet<EntityID>(worldInfo.getEntityIDsOfType(StandardEntityURN.BUILDING));
		
		registerModule(this.pathPlanning);

	}

	@Override
	public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
		modulesPrecompute(precomputeData);

	}

	@Override
	public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
			PrecomputeData precomputeData, DevelopData developData) {
		modulesResume(precomputeData);
		NRLTacticsUtilities.showTimestep(isVisualDebug,agentInfo, worldInfo, scenarioInfo);

	}

	@Override
	public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, DevelopData developData) {
		modulesPreparate();
		NRLTacticsUtilities.showTimestep(isVisualDebug,agentInfo, worldInfo, scenarioInfo);

	}

	@Override
	public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
		this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
		this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
		this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
		modulesUpdateInfo(messageManager);
		updateInfoFromSelf(agentInfo);
		updateDataFromCommuncication(worldInfo, messageManager);
		NRLTacticsUtilities.showTimestep(isVisualDebug,agentInfo, worldInfo, scenarioInfo);

		PoliceForce agent = (PoliceForce)agentInfo.me();
		Action action = null;
		return action;
	}
	
	
	private void updateInfoFromSelf(AgentInfo agentInfo) {
		if(agentInfo.getPositionArea().getStandardURN() == StandardEntityURN.ROAD) {
			this.unexploredRoads.remove(agentInfo.getPosition());
		}
		else if(agentInfo.getPositionArea().getStandardURN() == StandardEntityURN.BUILDING) {
			this.unexploredBuildings.remove(agentInfo.getPosition());
		}
		
	}

	private void updateDataFromCommuncication(WorldInfo worldInfo, MessageManager messageManager) {
		for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
			if (message instanceof MessageAmbulanceTeam
					|| message instanceof MessagePoliceForce
					|| message instanceof MessageFireBrigade) {
				EntityID senderPosition = NRLTacticsUtilities.getSenderPosition(message);
				if (worldInfo.getEntity(senderPosition).getStandardURN() == StandardEntityURN.ROAD) {
					this.unexploredRoads.remove(senderPosition);
				} else if (worldInfo.getEntity(senderPosition).getStandardURN() == StandardEntityURN.BUILDING) {
					this.unexploredBuildings.remove(senderPosition);
				}
			}
		}
		
	}

}
