package nrl.actorsim.roborescue;

import adf.agent.communication.MessageManager;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.module.complex.HumanDetector;
import adf.component.tactics.TacticsAmbulanceCentre;
import adf.debug.WorldViewLauncher;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

public class CentralizedPassthroughAmbulanceCenter extends TacticsAmbulanceCentre implements PassthroughDispatcher {
    final static private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CentralizedPassthroughAmbulanceCenter.class);

    private HumanDetector humanDetector;
    private Boolean isVisualDebug;
    private MessageTool messageTool;

    private boolean thisObjectIsTheDispatchUpdater = false;  //only one passthrough dispatcher should update the centeralized dispatcher; first one wins
    private CentralizedDispatcher dispatcher;
    private EntityID agentID;
    private String shortName;
    // -----------------------------------
    // TacticsLogabble Interface
    // -----------------------------------

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        worldInfo.indexClass(
                StandardEntityURN.CIVILIAN,
                StandardEntityURN.FIRE_BRIGADE,
                StandardEntityURN.POLICE_FORCE,
                StandardEntityURN.AMBULANCE_TEAM,
                StandardEntityURN.ROAD,
                StandardEntityURN.HYDRANT,
                StandardEntityURN.BUILDING,
                StandardEntityURN.REFUGE,
                StandardEntityURN.GAS_STATION,
                StandardEntityURN.AMBULANCE_CENTRE,
                StandardEntityURN.FIRE_STATION,
                StandardEntityURN.POLICE_OFFICE
        );

        this.messageTool = new MessageTool(scenarioInfo, developData);
        this.humanDetector = moduleManager.getModule("TacticsAmbulanceTeam.HumanDetector", "adf.sample.module.complex.SampleHumanDetector");
        registerModule(this.humanDetector);

        this.isVisualDebug = (scenarioInfo.isDebugMode()
                && moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));

        agentID = agentInfo.getID();
        String agentType = agentInfo.me().getStandardURN().name();
        shortName = agentType + "-" + agentID;

        if (CentralizedDispatcher.isInitialized == false) {
            RROptions rrOptions = new RROptions(agentInfo, worldInfo, scenarioInfo, moduleManager, messageManager, developData);

            dispatcher = CentralizedDispatcher.initialize(rrOptions);
            thisObjectIsTheDispatchUpdater = true;
        } else {
            dispatcher = CentralizedDispatcher.getInstance();
        }

        dispatcher.register(this);
        logger.info("{}: Initilialzed", shortName);
    }

    @Override
    public void think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager, MessageManager messageManager, DevelopData debugData) {
        this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        modulesUpdateInfo(messageManager);
        if (thisObjectIsTheDispatchUpdater) {
            dispatcher.update(this, worldInfo, messageManager);
        }

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }

        if (thisObjectIsTheDispatchUpdater) {
            dispatcher.addMessages(messageManager);
        }
        //Construct command messages for the agentTypeMap and add them to the messageManager
        //Map<EntityID, EntityID> allocatorResult = this.allocator.calc().getResult();
        //for (CommunicationMessage message : this.picker.setAllocatorResult(allocatorResult).calc().getResult()) {
        //    messageManager.addMessage(message);
        //}
    }

    @Override
    public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData debugData) {
        modulesResume(precomputeData);

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }
    }

    @Override
    public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager, DevelopData debugData) {
        modulesPreparate();

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }
    }

}
