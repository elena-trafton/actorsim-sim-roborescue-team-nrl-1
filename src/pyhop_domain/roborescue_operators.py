import copy
from roborescue_methods import Pair, get_distance_between_locations, get_distance_between_point_and_line_segment



def in_range(state, agent_id, target_id, range):
    loc1 = state.location[agent_id]
    loc2 = state.location[target_id]

    dist = get_distance_between_locations(loc1, loc2)

    return dist < range


def in_range_to_line(state, agent_id, point1_on_line, point2_on_line, range):
    agent_loc = state.location[agent_id]

    dist = get_distance_between_point_and_line_segment(agent_loc, point1_on_line, point2_on_line)

    return dist < range




##################################################
#
#    Operators available to all agents
#
##################################################

def move(state, agent_id, destination_id):
    state.position[agent_id] = destination_id
    state.location[agent_id] = state.location[destination_id]
    state.action[agent_id] = 'move'
    return state


def move_to_point(state, agent_id, destination_id, end_x, end_y):
    state.position[agent_id] = destination_id
    state.location[agent_id] = Pair(end_x, end_y)
    state.action[agent_id] = 'move'
    return state

def move_until_in_range(state, agent_id, destination_id, range):
    state.position[agent_id] = destination_id
    state.location[agent_id] = state.location[destination_id]
    state.action[agent_id] = 'move'
    return state


def rest(state, agent_id):
    state.action[agent_id] = 'rest'
    return state

##################################################
#
#    Operators available for the ambulance team
#
##################################################


def rescue(state, agent_id, target_id):
    if state.type[agent_id] != 'AMBULANCE_TEAM' or state.position[agent_id] != state.position[target_id] \
            or target_id not in state.buried:
        return False
    state.action[agent_id] = 'rescue'
    state.buried.remove(target_id)
    if state.type[target_id] == 'CIVILIAN':
        state.injured.add(target_id)
    return state


def load(state, agent_id, target_id):
    if state.type[agent_id] != 'AMBULANCE_TEAM' or state.type[target_id] != 'CIVILIAN' \
            or state.position[agent_id] != state.position[target_id] or target_id not in state.injured:
        return False
    state.injured.remove(target_id)
    state.action[agent_id] = 'load'
    state.on_board[agent_id] = target_id
    state.position[target_id] = agent_id
    state.location[target_id] = state.location[agent_id]
    return state


def unload(state, agent_id):
    if state.type[agent_id] != 'AMBULANCE_TEAM' or agent_id not in state.on_board:
        return False
    state.action[agent_id] = 'unload'
    target_id = state.on_board[agent_id]
    del state.on_board[agent_id]
    state.position[target_id] = state.position[agent_id]
    state.location[target_id] = state.location[state.position[target_id]]
    return state


##################################################
#
#    Operators available for the police force
#
##################################################


def clear(state, agent_id, clear_x, clear_y, target_blockade):
    if state.type[agent_id] != 'POLICE_FORCE':
        return False
    state.action[agent_id] = 'clear'
    return state


def clear_road(state, agent_id, end_position, end_x, end_y):
    if state.type[agent_id] != 'POLICE_FORCE':
        return False
    state.action[agent_id] = 'clear'
    state.position[agent_id] = end_position
    state.location[agent_id] = Pair(end_x, end_y)
    return state


def clear_current_area(state, agent_id):
    if state.type[agent_id] != 'POLICE_FORCE':
        return False
    state.action[agent_id] = 'clear'
    position = state.position[agent_id]
    former_stuck_agents = copy.deepcopy(state.stuck_agents)
    for stuck_agent in former_stuck_agents:
        if state.position[stuck_agent] == position:
            state.stuck_agents.remove(stuck_agent)

    former_stuck_civilians = copy.deepcopy(state.stuck_civilians)
    for stuck_civilian in former_stuck_civilians:
        if state.position[stuck_civilian] == position:
            state.stuck_civilians.remove(stuck_civilian)
    return state

##################################################
#
#    Operators available for the fire brigade
#
##################################################


def extinguish(state, agent_id, target_id, water_power):
    if state.type[agent_id] != 'FIRE_BRIGADE' or not in_range(state, agent_id, target_id, state.max_water_range) \
            or state.water[agent_id] < water_power:
        return False
    state.action[agent_id] = 'extinguish'
    state.water[agent_id] -= water_power
    state.fire_buildings.remove(target_id)
    return state


def refill(state, agent_id):
    if state.type[agent_id] != 'FIRE_BRIGADE' \
            or (state.position[agent_id] not in state.hydrants and state.position[agent_id] not in state.refuges):
        return False
    state.action[agent_id] = 'refill'
    return state
