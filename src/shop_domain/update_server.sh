
original_domain_file=domain_roborescue_full.lisp
pwd=`pwd`
original_domain_path=`dirname $pwd`/$original_domain_file
tmp_domain_file=tmp_roborescue_full.lisp
server_file=jshop2_server_dir/minecraft.lisp
echo "Updating server file: $server_file"

chmod 777 $server_file

echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" > $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";; This is an automatically generated file that should not be edited. " >> $tmp_domain_file
echo ";; Instead, edit $original_domain_path" >> $tmp_domain_file
echo ";; and run update_server.sh in the same directory to produce this file." >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" >> $tmp_domain_file
echo " " >> $tmp_domain_file
echo " " >> $tmp_domain_file
echo " " >> $tmp_domain_file
echo " " >> $tmp_domain_file

cat domain_notes.lisp >> $tmp_domain_file
echo ";; methods used in this file include " >> $tmp_domain_file
grep ":method" $original_domain_file | sed -ne 's/.*/;; &/p'  >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";;" >> $tmp_domain_file
echo ";; operators used in this file include " >> $tmp_domain_file
grep ":operator" $original_domain_file | sed -ne 's/.*/;; &/p'  >> $tmp_domain_file

cat $original_domain_file >> $tmp_domain_file

cp $tmp_domain_file $server_file
chmod 444 $server_file
