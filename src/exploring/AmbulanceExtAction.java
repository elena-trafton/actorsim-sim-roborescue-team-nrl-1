package exploring;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import adf.agent.Agent;
import adf.agent.action.Action;
import adf.agent.action.ambulance.ActionLoad;
import adf.agent.action.ambulance.ActionRescue;
import adf.agent.action.ambulance.ActionUnload;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.centralized.MessageReport;
import adf.agent.communication.standard.bundle.information.MessageBuilding;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.component.communication.CommunicationMessage;
import adf.component.extaction.ExtAction;
import adf.component.module.algorithm.PathPlanning;
import adf.sample.module.algorithm.SamplePathPlanning;
import rescuecore2.standard.entities.AmbulanceTeam;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.Civilian;
import rescuecore2.standard.entities.Human;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

import static rescuecore2.standard.entities.StandardEntityURN.*;


public class AmbulanceExtAction extends ExtAction{

	private PathPlanning pathPlanning;
	private EntityID target;
	private AmbulanceTeam agent;
	private List<EntityID> uncheckedBuildings;

	private EntityID buildingForMessage;
	private boolean isBuildingForMessageAdded;

	public AmbulanceExtAction(AgentInfo ai, WorldInfo wi, ScenarioInfo si, ModuleManager moduleManager,
			DevelopData developData) {
		super(ai, wi, si, moduleManager, developData);
		this.pathPlanning = new SamplePathPlanning(ai, wi, si, moduleManager, developData);
		this.target = null;
		this.agent = (AmbulanceTeam)this.agentInfo.me();
		this.uncheckedBuildings = new Vector<EntityID>(worldInfo.getEntityIDsOfType(BUILDING));
		this.buildingForMessage = null;
		this.isBuildingForMessageAdded = false;
		Collections.shuffle(this.uncheckedBuildings);
	}

	@Override
	public ExtAction setTarget(EntityID targets) {
		this.target = targets;
		return this;
	}

	@Override
	public ExtAction calc() {
		this.result = null;
		
		this.buildingForMessage = null;
		Human passenger = this.agentInfo.someoneOnBoard();
		if(passenger != null) {
			this.result = this.calcUnload(this.agent, this.pathPlanning, passenger, this.target);
			if(this.result != null) {
				return this;
			}
		}

		this.result = calcRescue(agent, this.pathPlanning, this.target);
		return this;
	}

	private Action calcUnload(AmbulanceTeam agent, PathPlanning pathPlanning, Human passenger, EntityID target) {
		if(passenger == null) {
			return null;
		}
		if(passenger.isHPDefined()
				&& passenger.getHP() == 0) {
			return new ActionUnload();
		}
		EntityID agentPosition = agent.getPosition();
		if(this.worldInfo.getEntity(agentPosition).getStandardURN() == StandardEntityURN.REFUGE) {
			return new ActionUnload();
		}
		Collection<EntityID> refuges = this.worldInfo.getEntityIDsOfType(StandardEntityURN.REFUGE);
		if(!refuges.isEmpty()) {
			List<EntityID> firstPath = null;
			for(EntityID refuge : refuges) {
				pathPlanning.setFrom(agentPosition);
				pathPlanning.setDestination(refuge);
				List<EntityID> path = pathPlanning.calc().getResult();
				if(path != null) {
					if(firstPath == null) {
						firstPath = path;
					}
					pathPlanning.setFrom(refuge);
					pathPlanning.setDestination(target);
					List<EntityID> returnToTargetPath = pathPlanning.calc().getResult();
					if(returnToTargetPath != null
							&& returnToTargetPath.size() > 0) {
						return new ActionMove(path);
					}
				}
			}
			return firstPath != null ? new ActionMove(firstPath) : null;
		}
		return null;
	}
	private Action calcRescue(AmbulanceTeam agent, PathPlanning pathPlanning, EntityID target) {
		StandardEntity agentLocation = this.worldInfo.getEntity(agent.getPosition());
		EntityID locationID = agentLocation.getID();
		if(agentLocation instanceof Building && !agentLocation.getStandardURN().equals(REFUGE)) {

			this.uncheckedBuildings.remove(locationID);
			this.buildingForMessage = locationID;
			this.isBuildingForMessageAdded = false;
			if(worldInfo.getNumberOfBuried(locationID) > 0) {
				Collection<Human> buriedPeople = this.worldInfo.getBuriedHumans(locationID);

				if(buriedPeople != null
						&& buriedPeople.size() > 0) {

					for(Human buriedPerson : buriedPeople) {
						if(buriedPerson.getHP() != 0) {
							return new ActionRescue(buriedPerson.getID());
						}
					}
				}

			}
			Collection<StandardEntity> people = this.worldInfo.getEntitiesOfType(CIVILIAN);
			for(StandardEntity person : people) {
				if(person instanceof Human) {

					Human human = (Human)person;
					if(human.getPosition().equals(locationID) && human.getHP() != 0) {
						this.uncheckedBuildings.add(locationID);
						this.isBuildingForMessageAdded = true;
						return new ActionLoad(human.getID());
					}
				}
			}
		}
		Action action = calcRescueRoute(agent, pathPlanning, target);
		return action;
	}
	private Action calcRescueRoute(AmbulanceTeam agent, PathPlanning pathPlanning, EntityID target) {
		EntityID agentPosition = agent.getPosition();
		Collection<EntityID> buildingIDs = uncheckedBuildings;
		if(!buildingIDs.isEmpty()) {
			List<EntityID> firstPath = null;

			for(EntityID buildingID : buildingIDs) {

				pathPlanning.setFrom(agentPosition);
				pathPlanning.setDestination(buildingID);
				List<EntityID> path = pathPlanning.calc().getResult();
				if(path != null) {
					if(firstPath == null) {
						firstPath = path;
					}
					pathPlanning.setFrom(buildingID);
					pathPlanning.setDestination(target);
					List<EntityID> returnToTargetPath = pathPlanning.calc().getResult();
					if(returnToTargetPath != null
							&& returnToTargetPath.size() > 0) {
						return new ActionMove(path);
					}
				}
			}
			return firstPath != null ? new ActionMove(firstPath) : null;
		}
		return null;

	}

	//This is possibly not the correct use of updateInfo or MessageReport and I will change this if I have to
	public ExtAction updateInfo(MessageManager messageManager) {
		for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
			if (message instanceof MessageReport) {
				MessageReport reportMessage = (MessageReport) message;
				if (!reportMessage.getSenderID().equals(this.agent.getID())
						&& this.worldInfo.getEntity(reportMessage.getSenderID()) instanceof AmbulanceTeam) {

					if (!reportMessage.isDone()) {
						this.uncheckedBuildings.remove(reportMessage.getFromID());
					} else {
						this.uncheckedBuildings.add(reportMessage.getFromID());
					}
				}
			}
		}
		
		this.pathPlanning.updateInfo(messageManager);
		if(this.buildingForMessage != null) {
			
			messageManager.addMessage(new MessageReport(true, this.isBuildingForMessageAdded, true, this.buildingForMessage));
			
		}
		return this;
	}

}


